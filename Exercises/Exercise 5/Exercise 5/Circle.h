#pragma once
#include "Shape.h"
#include <math.h>
#include <iostream>

const float PI = 3.14f;

class Circle : public Shape
{
public:
	Circle(float radius);

	float getArea();
	~Circle();

private:
	float mRadius;
};

