#pragma once
#include "Shape.h"
#include <math.h>
#include <iostream>

class Square : public Shape
{
public:
	Square(float length);

	float getArea();
	~Square();

private: 
	float mLength;
};

