#include <iostream>
#include <string>
#include <vector>
#include "Shape.h"
#include "Rectangle.h"
#include "Circle.h"
#include "Square.h"

using namespace std;

int main()
{
	vector<Shape> Shapes;

	Shapes.push_back(Circle(10.0f));
	Shapes.push_back(Rectangle(13.0f, 14.0f));
	Shapes.push_back(Square(1.412f));

	cout << "SHAPES" << endl;

	for (int i = 0; i < 3; i++)
	{
		cout << "Name: " << Shapes[i].getName();
		cout << "\t Sides: " << Shapes[i].getSides();
		cout << "\t Area: " << Shapes[i].getArea() << endl;
	}

	system("pause");
	return(0);
}