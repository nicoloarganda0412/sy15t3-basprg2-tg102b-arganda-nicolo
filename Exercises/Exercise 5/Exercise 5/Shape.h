#pragma once
#include <iostream>
#include <string>
#include <math.h>

using namespace std;

class Shape
{
public:
	Shape();
	string getName();
	int getSides();
	virtual float getArea();
	~Shape();

protected: 
	string mName;
	int mSides;
};

