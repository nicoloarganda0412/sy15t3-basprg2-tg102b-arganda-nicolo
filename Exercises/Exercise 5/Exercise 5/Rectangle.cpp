#include "Rectangle.h"

Rectangle::Rectangle(float width, float height)
{
	mName = "Rectangle";
	mSides = 4;
	mWidth = width;
	mHeight = height;
}

float Rectangle::getArea()
{
	return mWidth * mHeight;
}

Rectangle::~Rectangle()
{
}
