#pragma once
#include "Shape.h"
#include <math.h>
#include <iostream>

class Rectangle : public Shape
{
public:
	Rectangle(float width, float height);
	float getArea();


	~Rectangle();
private: 
	float mWidth;
	float mHeight;

};

