#include "Circle.h"

Circle::Circle(float radius)
{
	mName = "Circle";
	mSides = 0;
	mRadius = radius;
}

float Circle::getArea()
{
	return PI * pow(mRadius, 2);
}

Circle::~Circle()
{
}
