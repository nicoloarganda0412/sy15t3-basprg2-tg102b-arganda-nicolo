#include "Square.h"

Square::Square(float length)
{
	mName = "Square";
	mSides = 4;
	mLength = length;
}

float Square::getArea()
{
	return pow(mLength, 2);
}

Square::~Square()
{
}
