#include "Unit.h"
#include <time.h>



Unit::Unit()
{
}


Unit::~Unit()
{
}

string Unit::getName()
{
	return mName;
}

int Unit::getHp()
{
	return mHp;
}

int Unit::getMaxHp()
{
	return (rand()% 60 + 10) * 15;
}

int Unit::getMp()
{
	return mMp;
}

int Unit::getMaxMp()
{
	return (rand() % 60 + 10) * 15;
}

void Unit::takeDamage(int damage)
{
	mHp -= damage;
	if (mHp < 0)
	{
		mHp = 0;
	}
}

void Unit::useSkill(Skill* skill, Unit* target)
{
	skill->activate(this, target);
}

int Unit::getMagicAtk()
{
	srand(0);
	return ((rand() % 50 + 20) * 5 + (rand() % 50 + 10));
}

int Unit::getMagicDef()
{
	return (rand() % 60 + 10) * 10 + (rand() % 60 + 10);
}


