#include <iostream>
#include <string>
#include "Unit.h"
#include "Skill.h"

using namespace std;

int main()
{
	Unit* Player = new Unit;
	Unit* Enemy = new Unit;
	Skill* Blast = new Skill("Blast", 12.99, 2);

	while (Player->getHp() == 0 || Enemy->getHp() == 0)
	{
	
	cout << "PLAYER Health: " << Player->getHp() << endl;
	cout << "Enemy Health: " << Enemy->getHp() << endl;

	system("pause");
	system("cls");

	cout << "Player is attacking!" << endl;
	Player->useSkill(Blast, Enemy);
	
	system("pause");
	system("cls");

	cout << "Enemy is attacking!" << endl;
	Enemy->useSkill(Blast, Player);

	}

	system("pause");
	return(0);
}