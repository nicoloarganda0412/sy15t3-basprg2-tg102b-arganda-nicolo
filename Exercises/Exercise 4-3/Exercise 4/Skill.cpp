#include "Skill.h"



Skill::Skill(string name, float damageMultiplier, int mpCost)
{
	mName = name;
	mDamageMultiplier = damageMultiplier;
	mMpCost = mpCost;

}

Skill::~Skill()
{
}

string Skill::getName()
{
	return mName;
}

bool Skill::activate(Unit * actor, Unit * target)
{
	if (actor->getMp() < mMpCost)
	{
		return false;
	}

	int damage = actor->getMagicAtk() * mDamageMultiplier;
	damage -= actor->getMagicDef();
	target->takeDamage(damage);
	return true;
}
