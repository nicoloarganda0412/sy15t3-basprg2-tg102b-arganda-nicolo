#pragma once
#include <string>
#include "Skill.h"
using namespace std;


class Unit
{
public:
	Unit();
	~Unit();

	string getName();
	int getHp();
	int getMaxHp();
	int getMp();
	int getMaxMp();

	void takeDamage(int damage);

	void useSkill(Skill* skill, Unit* target);

	int getMagicAtk();
	int getMagicDef();

private:
	string mName;
	int mHp;
	int mMp;
	
};

