#include <iostream>
#include <string>

using namespace std;

struct roll
{
	string type;
	int rank;
	int value;
};

int payout(roll player, roll dealer, int bet, int& money)
{
	if (player.rank > dealer.rank)
		return bet;

	if (player.rank < dealer.rank)
		return bet * -1;

	return 0;
}

int main()
{
	int money = 90000;
	int bet;

	cout << "How much would you like to bet? ";
	cin >> bet;
	cout << money << endl;

	roll player;
	player.type = "pair";
	player.rank = 1;
	player.value = 5;

	roll dealer;
	dealer.type = "4-5-6";
	dealer.rank = 2;
	dealer.rank = 0;

	
	payout(player, dealer, bet, money);
	cout << money << endl;


}