#include <iostream>
#include <string>

using namespace std;

struct roll
{
	string type;
	int rank;
	int value;
};

void playWager(int& x)
{
	bool valid = false;
	int bet;
	while (!valid)
	{
		cout << "How much would you like to bet? ";
		cin >> bet;

		if (bet >= 100 && bet <= x)
		{
			valid = true;
		}

		else if (bet < 100)
		{
			cout << "Your bet needs to be higher than 100 perico" << endl;
			system("pause");
			system("cls");

		}
		else if (bet > x)
		{
			cout << "You don't have enough money to bet that amount." << endl;
			system("pause");
			system("cls");
		}
	}
}


int payout(roll player, roll dealer, int bet, int& money)
{
	if (player.rank > dealer.rank)
		return bet;

	if (player.rank < dealer.rank)
		return bet * -1;

	return 0;
}

int playRound(int round, int& money, roll player, roll dealer, int bet)
{
	playWager(money);
	payout(player, dealer, bet, money);
}

int main()
{
	int money = 90000;

	roll player;
	player.type = "pair";
	player.rank = 1;
	player.value = 5;

	roll dealer;
	dealer.type = "4-5-6";
	dealer.value = 2;
	dealer.value = 0;

	for (int i = 0; i < 10; i++)
	{
		playRound(i + 1, money, player, dealer, bet);
		cout << money << endl;
	}

	system("pause");
	return(0);
}