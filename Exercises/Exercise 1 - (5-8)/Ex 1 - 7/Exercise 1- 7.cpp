#include <iostream>
#include <string>

using namespace std;

int arrangeArray(int x[5])
{
	for (int i = 0; i < 5; i++)
	{
		int smallestIndex = i;
		for (int a = i + 1; a < 5; a++)
		{
			if (x[a] < x[smallestIndex])
			{
				smallestIndex = a;
			}

			int temp = x[smallestIndex];
			x[smallestIndex] = x[i];
			x[i] = temp;
		}
	}
	return x[5];
}

void printArray(int x[5])
{
	for (int i = 0; i < 5; i++)
	{
		cout << x[i] << ", ";
	}
}

int main()
{
	int arr[5] = { 2, 5, 1, 23, 76 };
	arr[5] = arrangeArray(arr);

	printArray(arr);
	system("pause");
	return 0;
}

