#pragma once
#include <string>
#include <iostream>

class Unit
{
public:
	Unit(int pow, int vit, int agi, int dex, int maxhp);
	~Unit();
	
	void viewStats();

	string getName();
	int getMaxHp();
	int getHp();
	int getPower();
	int getVitality();
	int getAgility();
	int getDexterity();

	void heal();
	void statIncrease();



private:
	string mName;
	int mPower, mVit, mAgi, mDex, mMaxHp, mHp;
};

