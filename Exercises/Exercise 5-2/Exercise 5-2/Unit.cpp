#include "Unit.h"
#include <string>
using namespace std;

Unit::Unit(int pow, int vit, int agi, int dex, int maxhp)
{

	mName = "Unit";
	mPower = pow;
	mVit = vit;
	mAgi = agi;
	mDex = dex;
	mMaxHp = maxhp;
	mHp = maxhp;
}

Unit::~Unit()
{
}

void Unit::viewStats()
{

	cout << "HP = " << mHp << "/" << mMaxHp << endl;
	cout << "POW = " << mPower << endl;
	cout << "VIT = " << mVit << endl;
	cout << "AGI = " << mAgi << endl;
	cout << "DEX = " << mDex << endl;

}

string Unit::getName()
{
	return mName;
}

int Unit::getMaxHp()
{
	return mMaxHp;
}

int Unit::getHp()
{
	return mHp;
}

int Unit::getPower()
{
	return mPower;
}

int Unit::getVitality()
{
	return mVit;
}

int Unit::getAgility()
{
	return mDex;
}

int Unit::getDexterity()
{
	return mDex;
}

void Unit::heal()
{
	if (mHp == mMaxHp)
	{
		cout << "Your HP is full" << endl;
	}

	else
	{
		mHp * .30;
	}
}

void Unit::statIncrease()
{
	mAgi += 2;
	cout << "Agility: " << mAgi << endl;
	mPower += 2;
	cout << "Power: " << mPower << endl;
	mVit += 2;
	cout << "Vitality: " << mAgi << endl;
	mDex += 2;
	cout << "Dexterity: " << mAgi << endl;
	mMaxHp += 2;
	cout << "MaxHp: " << mMaxHp << endl;


}

