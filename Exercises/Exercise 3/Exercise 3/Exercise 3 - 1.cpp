#include <iostream>
#include <string>
#include <time.h>

using namespace std; 

void fillArray(int* random, int size)
{
	srand(time(0));
	for (int i = 0; i < size; i++)
	{
		*(random + i) = (rand() % 100 + 1);
	}
}

void printArray(int* numbers, int size)
{
	for (int i = 0; i < size; i++)
	{
		cout << *(numbers + i);
		cout << endl;
	}
}
int main()
{
	int* random = new int [10];

	// Fill Array.
	fillArray(random, 10);
	printArray(random, 10);

	delete[] random;
	random = NULL;
	system("pause");
	return 0;
}