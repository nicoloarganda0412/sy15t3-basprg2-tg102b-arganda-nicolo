#include <iostream>
#include <string>
#include <time.h>

using namespace std;

int* createRandom(int size)
{
	srand(time(0));
	int* randomArray = new int[size];
	for (int i = 0; i < size; i++)
	{
		*(randomArray + i) = (rand() % 100 + 1);
	}
	return randomArray;
}

void printArray(int* numbers, int size)
{
	for (int i = 0; i < size; i++)
	{
		cout << *(numbers + 1) << endl;
	}
}

int main()
{
	int* randomArray = createRandom(10);
	printArray(randomArray, 10);	
	delete[] randomArray;

	system("pause");
	return (0);
}