#include <iostream>
#include <string>
#include <time.h>

using namespace std;

struct Roll
{
	int dice[3];
};

Roll* rollDice()
{
	srand(time(0));
	Roll* roll = new Roll;
	for (int i = 0; i < 4; i++)
	{
		roll->dice[i] = rand() % 6 + 1;

		cout << "Dice " << i << ": " << roll->dice[i] << ", ";
	}

	cout << endl;
	return roll;
}

void playRound()
{
	for (int a = 0; a < 3; a++)
	{
		Roll* roll = rollDice();
		delete roll;
	}
}


int main()
{
	
	for (int i = 0; i < 10; i++)
	{
		playRound();
	}

	system("pause");
	return (0);

}