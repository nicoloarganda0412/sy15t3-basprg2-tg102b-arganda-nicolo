#include <iostream>
#include <string>
#include <time.h>
#define NODE_H

using namespace std;

struct Node
{
	std::string name;
	Node* next = NULL;
	Node* previous = NULL;
};

Node* creatingCircularList()
{
	Node* header = NULL;
	Node* current = NULL;
	Node* previous = NULL;

	// Nth Node is not being read.
	// fix the input for how many players wanted.
	current = new Node;
	cout << "What is your name soldier? ";
	getline(cin, current->name);
	previous = current;
	header = current;

	current = new Node;
	cout << "What is your name soldier? ";
	getline(cin, current->name);
	previous->next = current;
	previous = current;

	current = new Node;
	cout << "What is your name soldier? ";
	getline(cin, current->name);
	previous->next = current;
	previous = current;

	current = new Node;
	cout << "What is your name soldier? ";
	getline(cin, current->name);
	previous->next = current;
	previous = current;

	current = new Node;
	cout << "What is your name soldier? ";
	getline(cin, current->name);
	previous->next = current;

	return header;

}

Node* deletePlayer(Node* header)
{
	Node* prevTemp = NULL;
	prevTemp = header;
	cout << header->name << " is out of the game!";
	header = header->next;
	delete prevTemp;

	return header;
}

void chooseRandom(Node* header, int playerCount)
{
	//First header is being deleted.

	srand(time(0));
	if (header == NULL)
	{
		delete(header);
	}

	string result = header->name;
	Node* previous = new Node;


	int random = rand() % playerCount + 1;
	cout << header->name << " rolled " << random << "!" << endl;

	if (random == 0)
	{
		result = header->name;
	}
	else
	{
	
		for (int b = 1; b <= random; b++)
		{
			header = header->next;
		}
	}
	//cout << "Supposed to be deleted: " << header->name << endl;
	//return header;

	deletePlayer(header);

}

void printPlayers(Node* header, int playerCount)
{
	cout << "Remaining Players: " << endl;
	for (int a = 0; a < playerCount; a++)
	{
		cout << header->name << endl;
		header = header->next;
	}
}

int main()
{
	Node* select = new Node;

	select = creatingCircularList();
	int playerCount = 5;
	int roundCount = 1;
	cout << endl;
	system("pause");
	system("cls");

	while (playerCount > 1)
	{
		cout << "Round " << roundCount << "!" << endl;
		printPlayers(select, playerCount);
		cout << endl;

		chooseRandom(select, playerCount);

		roundCount++;
		system("pause");
		system("cls");
	}

	system("pause");
	return (0);

}