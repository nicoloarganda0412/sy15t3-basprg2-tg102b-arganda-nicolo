#pragma once
#include <string>

using namespace std;

class Unit
{
public:
	Unit(string name, int maxhp, int hp, int power, int vitality, int agility, int dexterity, int job);
	~Unit();

	void viewStats(Unit* target);

	int getJob();
	int getMaxHp();
	int getHp();
	int getPower();
	int getVitality();
	int getAgility();
	int getDexterity();

	void StatIncrease(Unit* target);


	void increaseAllStats();


	string getName();
	bool landAttack(Unit* player, Unit* target);
	void damage(Unit* player, Unit* target);
	void heal();

private:
	string mName;
	int mMaxHitPoints;
	int mHitPoints;
	int mPower;
	int mVitality;
	int mAgility;
	int mDexterity;
	int mClass;

};

