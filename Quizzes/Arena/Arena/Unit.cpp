#include "Unit.h"
#include <string>
#include <iostream>
#include <vector>

using namespace std;


Unit::Unit(string name,int maxhp, int hp, int power, int vitality, int agility, int dexterity, int job)
{
	mName = name;
	mMaxHitPoints = maxhp;
	mHitPoints = hp;
	mPower = power;
	mVitality = vitality;
	mAgility = agility;
	mDexterity = dexterity;
	mClass = job;

}

Unit::~Unit()
{

}


void Unit::viewStats(Unit* target)
{
	cout << "Name = " << mName << endl;
	if (target->mClass == 1)
	{
		cout << "CLASS: WARRIOR " << endl;
	}
	if (target->mClass == 2)
	{
		cout << "CLASS: MAGE " << endl;
	}
	if (target->mClass == 3)
	{
		cout << "CLASS: ASSASSIN " << endl;
	}
	cout << "HP = " << mHitPoints << "/" << mMaxHitPoints << endl;
	cout << "POW = " << mPower << endl;
	cout << "VIT = " << mVitality << endl;
	cout << "AGI = " << mAgility << endl;
	cout << "DEX = " << mDexterity << endl;
}

int Unit::getJob()
{
	return mClass;
}

int Unit::getMaxHp()
{
	return mMaxHitPoints;
}

int Unit::getHp()
{
	return mHitPoints;
}

int Unit::getPower()
{
	return mPower;
}

int Unit::getVitality()
{
	return mVitality;
}

int Unit::getAgility()
{
	return mAgility;
}

int Unit::getDexterity()
{
	return mDexterity;
}

void Unit::StatIncrease(Unit* target)
{
	if (target->getJob() == 1)
	{
		mMaxHitPoints += 3;
		mVitality += 3;
	}

	else if (target->getJob() == 2)
	{
		mPower += 5;
	}

	else if (target->getJob() == 3)
	{
		mAgility += 3;
		mDexterity += 3;
	}

}



void Unit::increaseAllStats()
{
	mAgility += 1;
	mPower += 1;
	mDexterity += 1;
	mVitality += 1;
	mMaxHitPoints += 1;
	mHitPoints = mMaxHitPoints;
}

string Unit::getName()
{
	return mName;
}

bool Unit::landAttack(Unit* doer, Unit * target)
{
	int hit = doer->getDexterity() / target->getAgility();

	if (hit < 20)
	{
		hit = 20;
	}
	if (hit > 80)
	{
		hit = 80;
	}

	int dice = rand() % 100 + 1;

	if (dice <= hit)
	{
		return true;
	}
	else
	{
		return false;
	} 
}

void Unit::damage(Unit* player, Unit* target )
{
	int damage = 0;
	if (player->getJob() == 1 && target->getJob() == 2)
	{
		damage = (player->getPower() * target->getVitality()) * .5;
	}
	else if (player->getJob() == 3 && target->getJob() == 2)
	{
		damage = (player->getPower() * target->getVitality()) * .5;
	}
	else if (player->getJob() == 2 && target->getJob() == 1)
	{
		damage = (player->getPower() * target->getVitality()) * .5;
	}
	else
	{
		damage = (player->getPower() * target->getVitality());
	}

	target->mHitPoints -= damage;
	cout << player->getName() << " hit " << target->getName() << " for " << damage << " points! " << endl;
	if (target->mHitPoints < 0)
	{
		target->mHitPoints = 0;
	}
}

void Unit::heal()
{
	mHitPoints += (mMaxHitPoints * .3);

	if (mHitPoints > mMaxHitPoints)
	{
		mHitPoints = mMaxHitPoints;
	}
}


