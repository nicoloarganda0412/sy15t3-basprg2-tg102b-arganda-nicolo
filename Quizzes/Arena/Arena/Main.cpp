#include <iostream>
#include <string>
#include <time.h>
#include "Unit.h"

using namespace std;

//Bonus Damage



int main()
{
	srand(time(0));
	int maxHp = 5;
	int hp = maxHp;
	int vit = 2;
	int pow = 2;
	int dex = 5;
	int agi = 4;

	int aMaxHp = 3;
	int aHP = aMaxHp;
	int aVit = 2;
	int aPow = 2;
	int aDex = 5;
	int aAgi = 3;

	bool condition = true;

	string name;
	cout << "What is your name? " << endl;
	cin >> name;

	int job;
	cout << "What is your class? " << endl;
	cout << "Warrior  [1]" << endl;
	cout << "Mage	 [2]" << endl;
	cout << "Assassin [3]" << endl;
	cin >> job;
	system("pause");
	system("cls");

	//ROUND START
	int round = 1;
	Unit* player = new Unit(name, maxHp, hp, pow, vit, agi, dex, job);
	while(player->getHp() != 0)
	{
		int aJob = rand() % 3 + 1;
		Unit* enemy = new Unit("Enemy",aMaxHp, aHP, aPow, aVit, aAgi, aDex, aJob);

		if (round != 1)
		{
			for (int i = 1; i < round + 1; i++)
			{
				enemy->increaseAllStats();
			}
		}
		cout << "Round " << round << endl;
		player->viewStats(player);
		cout << endl;

		cout << "VS" << endl;
		cout << endl;

		enemy->viewStats(enemy);
		cout << endl;

		if (player->getAgility() > enemy->getAgility())
		{
			cout << player->getName() << " goes first!" << endl;
		}
		else
		{
			cout << enemy->getName() << " goes first!" << endl;
		}
		system("pause");
		system("cls");

		for (;;)
		{
			//ROUND
			cout << player->getName() << " HP: " << player->getHp() << "/" << player->getMaxHp() << endl;
			cout << enemy->getName() << " HP: " << enemy->getHp() << "/" << enemy->getMaxHp() << endl;

			if (player->getAgility() > enemy->getAgility() || player->getAgility() == enemy->getAgility())
			{

				//PLAYER ATTACKS
				if (player->landAttack(player, enemy))
				{
					cout << player->getName() << " landed the attack!" << endl;
					enemy->damage(player, enemy);
				}
				else
				{
					cout << player->getName() << " missed the attack!" << endl;
				}

				if (enemy->getHp() == 0)
				{
					cout << "You won!!!" << endl;
					system("pause");
					system("cls");
					break;
				}

				//ENEMY ATTACKS
				if (enemy->landAttack(player, enemy))
				{
					cout << enemy->getName() << " landed the attack!" << endl;
					player->damage(enemy, player);
				}
				else
				{
					cout << enemy->getName() << " missed the attack!" << endl;
				}
				system("pause");
				system("cls");
				if (player->getHp() == 0)
				{
					cout << "You lost!!!" << endl;
					condition = false;
					system("pause");
					system("cls");
					break;
				}
			}

			else
			{
				//ENEMY ATTACKS
				if (enemy->landAttack(player, enemy))
				{
					cout << enemy->getName() << " landed the attack!" << endl;
					player->damage(enemy, player);
				}
				else
				{
					cout << enemy->getName() << " missed the attack!" << endl;
				}
			
				if (player->getHp() == 0)
				{
					cout << "You lost!!!" << endl;
					condition = false;
					system("pause");
					system("cls");
					break;
				}

				//PLAYER ATTACKS
				if (player->landAttack(player, enemy))
				{
					cout << player->getName() << " landed the attack!" << endl;
					enemy->damage(player, enemy);
				}
				else
				{
					cout << player->getName() << " missed the attack!" << endl;
				}

				if (enemy->getHp() == 0)
				{
					cout << "You won!!!" << endl;
					system("pause");
					system("cls");
					break;
				}
			}
			system("pause");
			system("cls");
		}

		if (condition == true)
		{
			player->StatIncrease(enemy);
			delete enemy;

			if (player->getHp() != player->getMaxHp())
			{
				player->heal();
			}
		}

		system("pause");
		system("cls");
		round++;
	}
	//DETERMINE ENDING
	cout << "You survived for " << round << " rounds! " << endl;
	delete player;
	system("pause");
	return (0);
}
