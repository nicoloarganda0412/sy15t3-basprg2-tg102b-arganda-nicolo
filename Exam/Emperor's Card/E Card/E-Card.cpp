#include <iostream>
#include <vector>
#include <string>
#include <time.h>

using namespace std;

void printCards(vector<string> deck)
{
	cout << "Remaining Deck: " << endl;
	for (int i = 0; i < deck.size(); i++)
	{
		cout << deck[i] << " [" << i << "]" << endl;
	}
}

void evaluateCards(string player, string enemy, bool &condition, bool &win)
{
	// WIN CONDITION

	if (player == "Emperor" && enemy == "Citizen")
	{
		cout << "Kaiji wins this round" << endl;
		condition = 1;
		win = 1;
	}

	else if (player == "Citizen" && enemy == "Slave")
	{
		cout << "Kaiji wins this round" << endl;
		condition = 1;
		win = 1;
	}

	else if (player == "Slave" && enemy == "Emperor")
	{
		cout << "Kaiji wins this round" << endl;
		condition = 1;
		win = 1;
	}

	else if (player == "Emperor" && enemy == "Slave")
	{
		cout << "Kaiji loses this round" << endl;
		condition = 1;
	}
	// LOSE CONDITION
	else if (player == "Slave" && enemy == "Citizen")
	{
		cout << "Kaiji loses this round" << endl;
		condition = 1;
	}

	else if (player == "Citizen" && enemy == "Emperor")
	{
		cout << "Kaiji loses this round" << endl;
		condition = 1;
	}

	else if (player == "Citizen" && enemy == "Citizen")
	{
		cout << "It's a draw!" << endl;
		condition = 0;
	}

	system("pause");
	system("cls");
}

void playBet(int millimeters, int& wager)
{
	bool valid = false;
	
	while (!valid)
	{
		cout << "How much millimeters would you like to bet? ";
		cin >> wager;

		if (wager <= millimeters)
		{
			valid = true;
		}

		else if (wager < 1)
		{
			cout << " You need to wager higher than 1mm. ";
			system("pause");
			system("cls");
		}

		else if (wager > millimeters)
		{
			cout << "You can't wager that amount of distance";
			system("pause");
			system("cls");
		}
	}
}

void payout(int& millimeters, int& money, int wager, bool win, string playerChoice)
{
	if (win == 1)
	{
		if (playerChoice == "Slave")
		{
			money += (wager * 500000);
			cout << "You have earned " << wager * 500000;
		}
		else
		{
			money += (wager * 100000);
			cout << "You have earned " << wager * 100000;
		}
	}

	// LOSE CONDITION
	else if (win == 0)
	{
		millimeters -= wager;
		cout << "The drill moves " << wager << "mm!";
		cout << endl;
		cout << "Kaiji: GaaaaHH... AAaaaaah" << endl;
	}
}

void determineEnding(int millimeters, int money)
{
	//BEST ENDING
	if (millimeters > 0 && money >= 2000000)
	{
		cout << "YOU SPARED YOUR EARDRUM AND REACHED 20 MILLION YEN!" << endl;
		cout << endl;
		cout << "TONEGAWA BOWS IN SHAME." << endl;
	}
	else if (millimeters > 0 && money <= 2000000)
	{
		cout << "YOU SPARED YOUR EARDRUM BUT YOU DID NOT REACH 2000000 YEN" << endl;
	}
	else if (millimeters <= 0)
	{
		cout << "YOUR HAVE LOST YOUR EARDRUM AND THE MONEY YOU HAVE EARNED DOES NOT MATTER." << endl;
		cout << endl;
		cout << "TONEGAWA WILL NOT BOW TO A LOSER LIKE YOU." << endl;
	}
}

int main()
{
	srand(time(0));
	int pick;
	string playerChoice;
	string enemyChoice;
	bool condition;
	bool win = 0;
	int money = 0;
	int wager = 0;
	int millimeters = 30;
	int round = 1;
	while(millimeters > 0 && round <= 12)
	{
		vector<string> Emperor;
		Emperor.push_back("Emperor");
		Emperor.push_back("Citizen");
		Emperor.push_back("Citizen");
		Emperor.push_back("Citizen");
		Emperor.push_back("Citizen");

		vector<string> Slave;
		Slave.push_back("Slave");
		Slave.push_back("Citizen");
		Slave.push_back("Citizen");
		Slave.push_back("Citizen");
		Slave.push_back("Citizen");

		win = 0;
		condition = 0;
		cout << "Cash: " << money << endl;
		cout << "Distance left (mm): " << millimeters << endl;
		cout << "Round: " << round << "/12" << endl;
		if (round <= 3 || round >= 7 && round <= 9)
		{
			cout << "Side: Emperor" << endl;
		}
		else
		{
			cout << "Side: Slave" << endl;
		}

		int randomNum;
		playBet(millimeters, wager);

		system("pause");
		system("cls");

		while (condition == 0)
		{
			
			int randomNum = rand() % Slave.size();
			if (round <= 3 || round >= 7 && round <= 9)
			{
				printCards(Emperor);
			}
			else
			{
				printCards(Slave);
			}

			// Choosing a card.
			cout << "What card would you like to use? " << endl;
			cin >> pick;

			system("pause");
			system("cls");

			if (round <= 3 || round >= 7 && round <= 9)
			{
				playerChoice = Emperor[pick];
			}
			else
			{
				playerChoice = Slave[pick];
			}

			cout << "You have chosen " << playerChoice << endl;

			cout << endl;
			if (round <= 3 || round >= 7 && round <= 9)
			{
				enemyChoice = Slave[randomNum];
			}
			else
			{
				enemyChoice = Emperor[randomNum];
			}

			cout << "Tonegawa chose " << enemyChoice << endl;
			cout << endl;

			evaluateCards(playerChoice, enemyChoice, condition, win);

			Emperor.erase(Emperor.begin() + pick);
			Slave.erase(Slave.begin() + randomNum);

			//cout << "Slave size: " << Slave.size() << endl;
			//cout << "Emperor size: " << Emperor.size() << endl;
			
		}
		payout(millimeters, money, wager, win, playerChoice);
		cout << endl;
		system("pause");
		system("cls");
		round++;
	}

	determineEnding(millimeters, money);

	system("pause");
	return(0);

}