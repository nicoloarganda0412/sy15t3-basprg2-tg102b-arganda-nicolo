#pragma once
#include "Skill.h"
#include "MageHeal.h"
#include "WarriorShockwave.h"
#include "AssassinAssassinate.h"
#include <string>
#include <iostream>
#include <vector>

using namespace std;

class Unit
{
public:
	Unit(string name, string side, int job);

	string getName();
	string getSide();
	string getSkillName();

	int computeDamage(Unit* player, Unit* target);
	int getMaxHp();
	int getHp();
	int getMaxMp();
	int getMp();
	int getPow();
	int getVit();
	int getAgi();
	int getDex();
	int getJob();
	int getManaCost();


	bool alive();
	bool landAttack(Unit* doer, Unit* target);

	void showStats();
	void takeDamage(int damage);
	void heal();
	void skill(Unit* player, vector<Unit*> &target);
	void decreaseMp(int value);


	~Unit();

private:

	Skill *mSkill;

	string mName;
	string mSide;

	int mPow;
	int mVit;
	int mAgi; 
	int mDex;
	int mMaxHp;
	int mMaxMp;
	int mMp;
	int mHp;
	int mJob;

};

