#include <iostream>
#include "Unit.h"
#include "Skill.h"
#include "MageHeal.h"
#include "WarriorShockwave.h"
#include "AssassinAssassinate.h"


Unit::Unit(string name, string side, int job)
{
	mName = name;
	mSide = side;
	mJob = job;

	mPow = rand() % 19 + 30;
	mVit = rand() % 9 + 20;
	mDex = rand() % 9 + 30;
	mAgi = rand() % 9 + 30;

	mMaxHp = rand() % 50 + 20;
	mMaxMp = rand() % 15 + 5;

	mHp = mMaxHp;
	mMp = mMaxMp;

	if (job == 1)
	{
		mSkill = new WarriorShockwave("Shockwave");
	}
	else if (job == 2)
	{
		mSkill = new MageHeal("Heal");
	}
	else if (job == 3)
	{
		mSkill = new AssassinAssassinate("Assassinate");
	}
}

string Unit::getName()
{
	return mName;
}

string Unit::getSide()
{
	return mSide;
}

string Unit::getSkillName()
{
	return mSkill->getName();
}

int Unit::computeDamage(Unit * player, Unit * target)
{
	int var = mPow * .20;
	int damage = rand() % var + mPow;

	if (player->getJob() == 1 && target->getJob() == 2)
	{
		damage = (damage - target->getVit()) * 1.5;
	}
	else if (player->getJob() == 2 && target->getJob() == 3)
	{
		damage = (damage - target->getVit()) * 1.5;
	}
	else if (player->getJob() == 3 && target->getJob() == 1)
	{
		damage = (damage - target->getVit()) * 1.5;
	}
	else
	{
		damage = (damage - target->getVit());
	}

	if (damage < 1)
	{
		damage = 1;
	}
	return damage;

}

int Unit::getMaxHp()
{
	return mMaxHp;
}

int Unit::getHp()
{
	return mHp;
}

int Unit::getMaxMp()
{
	return mMaxMp;
}

int Unit::getMp()
{
	return mMp;
}

int Unit::getPow()
{
	return mPow;
}

int Unit::getVit()
{
	return mVit;
}

int Unit::getAgi()
{
	return mAgi;
}

int Unit::getDex()
{
	return mDex;
}

int Unit::getJob()
{
	return mJob;
}

int Unit::getManaCost()
{
	int cost = mSkill->getCost();
	return cost;
}

bool Unit::alive()
{
	if (mHp > 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Unit::landAttack(Unit* doer, Unit* target)
{
	int hit = (doer->getDex() / target->getAgi()) * 100;

	if (hit < 20)
	{
		hit = 20;
	}
	if (hit > 80)
	{
		hit = 80;
	}

	int dice = rand() % 100 + 1;

	if (dice <= hit)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void Unit::showStats()
{
	
	if (mJob == 1)
	{
		cout << "CLASS: Warrior " << endl;
	}
	else if (mJob == 2)
	{
		cout << "CLASS: Mage " << endl;
	}
	else if (mJob == 3)
	{
		cout << "CLASS: Assassin " << endl;
	}

	cout << "HP = " << mHp << "/" << mMaxHp << endl;
	cout << "POW = " << mPow << endl;
	cout << "VIT = " << mVit << endl;
	cout << "AGI = " << mAgi << endl;
	cout << "DEX = " << mDex << endl;
	cout << "MP = " << mMp << endl;
}

void Unit::takeDamage(int damage)
{
	mHp -= damage;
	if (mHp < 0)
	{
		mHp = 0;
	}
}

void Unit::heal()
{
	
	mHp += mMaxHp * .30;

	if (mHp > mMaxHp)
	{
		mHp = mMaxHp;
	}

}

void Unit::skill(Unit * player, vector<Unit*>& targets)
{
	mSkill->use(player, targets);
}

void Unit::decreaseMp(int value)
{
	mMp = mMp - value;
}


Unit::~Unit()
{
	delete mSkill;
}
