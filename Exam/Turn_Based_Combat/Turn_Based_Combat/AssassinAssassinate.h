#pragma once
#include "Skill.h"
#include <string>
#include <iostream>

class Unit;

using namespace std;

class AssassinAssassinate : public Skill
{
public:
	AssassinAssassinate(string name);

	void use(Unit* doer, vector<Unit*> &targets);

	~AssassinAssassinate();

private:
	float mDamageCoefficient;
};

