#include "MageHeal.h"
#include "Unit.h"


MageHeal::MageHeal(string name) :Skill(name)
{
	mCost = 2;
}

void MageHeal::use(Unit * doer, vector<Unit*>& targets)
{

	vector<Unit*> range;
	for (int x = 0; x < targets.size(); x++)
	{
		if (doer->getSide() == targets[x]->getSide())
		{
			range.push_back(targets[x]);
		}
	}

	Unit* target = range[0];

	for (int i = 0; i < range.size(); i++)
	{
		if (range[i]->getHp() <= target->getHp())
		{
			target = range[i];

		}
	}
	cout << doer->getName() << " is casting " << doer->getSkillName() << " on " << target->getName() << endl;
	if (target->getHp() <= target->getMaxHp())
	{
		target->heal();
		cout << doer->getName() << " healed " << target->getName() << endl;

	}
	else
	{
		cout << "No one doesn't need to heal. " << endl;
	}

	int cost = getCost();

	doer->decreaseMp(cost);

}


MageHeal::~MageHeal()
{
}
