#pragma once
#include "Skill.h"
#include <string>
#include <iostream>

class Unit;

using namespace std;

class MageHeal : public Skill
{
public:
	MageHeal(string name);

	void use(Unit* doer, vector<Unit*> &targets);

	~MageHeal();
private:
	int mHealingMulti;
};

