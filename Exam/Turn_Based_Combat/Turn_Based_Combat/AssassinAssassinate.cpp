#include "AssassinAssassinate.h"
#include "Unit.h"

AssassinAssassinate::AssassinAssassinate(string name):Skill (name)
{
	mCost = 2;
	mDamageCoefficient = 2.2f;
}

void AssassinAssassinate::use(Unit * doer, vector<Unit*>& targets)
{
		vector<Unit*> enemies;
		
		for (int a = 0; a < targets.size(); a++)
		{
			if (doer->getSide() != targets[a]->getSide())
			{
				enemies.push_back(targets[a]);
			}
		}

		Unit* target = enemies[0];

		for (int i = 0; i < enemies.size(); i++)
		{
			if (target->getHp() >= enemies[i]->getHp() && doer->getSide() != enemies[i]->getSide())
			{
				target = enemies[i];
			}
		}
		int damage = doer->computeDamage(doer, target) * mDamageCoefficient;
		target->takeDamage(damage);

		cout << doer->getName() << " is casting " << doer->getSkillName() << " on " << target->getName() << endl;
		cout << target->getName() << " took " << damage << " damage. " << endl;

		int cost = getCost();

		doer->decreaseMp(cost);
	
}


AssassinAssassinate::~AssassinAssassinate()
{
}
