#include "Unit.h"
#include "Skill.h"
#include "AssassinAssassinate.h"
#include "MageHeal.h"
#include "WarriorShockwave.h"
#include <iostream>
#include <string>
#include <vector>
#include <time.h>
#include <algorithm>

// Comments:
// determine Winner
// balance the game
// 
class Unit;
class Skill;

using namespace std;

vector<Unit*> Order(vector<Unit*> &sideA, vector<Unit*> &sideB);

void showSides(vector<Unit*> &side);
void printTurnOrder(vector<Unit*> &turn);
void playTurn(vector<Unit*> player, vector<Unit*> enemy);
void playerTurn(Unit* player, vector<Unit*> &targets);
void enemyTurn(Unit* enemy, vector<Unit*> &targets);

int main()
{
	srand(time(0));

	string playerTeamName = "Evangelions";
	string enemyTeamName = "Spirals";

	vector<Unit*> sideA;
	Unit* a = new Unit("Shinji", playerTeamName, 1);
	sideA.push_back(a);
	Unit* b = new Unit("Asuka", playerTeamName, 2);
	sideA.push_back(b);
	Unit* c = new Unit("Rei", playerTeamName, 3);
	sideA.push_back(c);

	vector<Unit*> sideB;
	Unit* x = new Unit("Simon", enemyTeamName, 1);
	sideB.push_back(x);
	Unit* y = new Unit("Yoko", enemyTeamName, 2);
	sideB.push_back(y);
	Unit* z = new Unit("Kamina", enemyTeamName, 3);
	sideB.push_back(z);

	playTurn(sideA, sideB);
	for (int g = 0; g < sideA.size(); g++)
	{
		delete sideA[g];
		delete sideB[g];
	}

	sideA.erase(sideA.begin(), sideA.end());
	sideB.erase(sideB.begin(), sideB.end());

	system("pause");
	return (0);

}
 
vector<Unit*> Order(vector<Unit*> &sideA, vector<Unit*> &sideB)
{
	vector<Unit*> turnOrder;

	for (int a = 0; a < 3; a++)
	{
		turnOrder.push_back(sideA[a]);
	}

	for (int b = 0; b < 3; b++)
	{
		turnOrder.push_back(sideB[b]);
	}

	sort(turnOrder.begin(), turnOrder.end(), [](Unit* a, Unit* b) {return a->getAgi() > b->getAgi(); });

	return turnOrder;
}

void printTurnOrder(vector<Unit*> &turn)
{
	cout << "======== oooTURN ORDERooo ========" << endl;
	for (int a = 0; a < turn.size(); a++)
	{
		cout << "#" << a + 1 << " [" << turn[a]->getSide() << "]"
			<< turn[a]->getName() << endl;
	}
	cout << "======== oooTURN ORDERooo ========" << endl;
}

Unit* findTarget(Unit* doer, vector<Unit*> &targets)
{
	vector<Unit*> enemies;
	for (int a = 0; a < targets.size(); a++)
	{
		if (doer->getSide() != targets[a]->getSide())
		{
			enemies.push_back(targets[a]);
		}

	}

	Unit* target = enemies[rand() % enemies.size()];
	return target;
}

bool isCrit(Unit* doer, Unit* target)
{

	int crit = 20;

	int dice = rand() % 100 + 1;

	if (dice <= crit)
	{
		return true;
	}
	else
	{
		return false;
	}

}

void playerTurn(Unit* player, vector<Unit*> &targets)
{

	int choice;

	cout << "Choose an action... " << endl;
	cout << "=============================" << endl;
	cout << endl;

	cout << "\t [1] Basic Attack" << endl;
	cout << "\t [2] " << player->getSkillName() << "(MP COST: " << player->getManaCost() << ")" << endl;
	for (;;)
	{
		cin >> choice;

		if (choice == 1)
		{
			Unit* target = findTarget(player, targets);
			int damage = player->computeDamage(player, target);

			if (player->landAttack(player, target))
			{
				if (isCrit(player, target))
				{
					damage *= 1.20;
					cout << " DEALING CRITICAL DAMAGE" << endl;

				}

				cout << player->getName() << " is attacking " << target->getName() << " for " << damage << " damage! " << endl;
				target->takeDamage(damage);

			}
		

			else
			{
				cout << "It's a miss! " << endl;
			}
			break;
		}

		else if (choice == 2)
		{
			if (player->getManaCost() > player->getMp())
			{
				cout << "You do not have enough Mana for that. " << endl;
			}
			else
			{ 
				player->skill(player, targets);
				break;
			}
		}
		else
		{
			cout << "Invalid Input" << endl;
		}
	}
}

void enemyTurn(Unit * enemy, vector<Unit*>& targets)
{

	int choice = 1;
	if (enemy->getMp() >= enemy->getManaCost())
	{
		choice = rand() % 2 + 1;
	}

	if (choice == 1)
	{
		Unit* target = findTarget(enemy, targets);
		int damage = enemy->computeDamage(enemy, target); 
		if (enemy->landAttack(enemy, target))
		{
			if (isCrit(enemy, target))
			{
				damage *= 1.20;
				cout << " DEALING CRITICAL DAMAGE" << endl;

			}
			cout << enemy->getName() << " is attacking " << target->getName() << " for " << damage << " damage! " << endl;

			target->takeDamage(damage);
		}
		else
		{
			cout << "It's a miss! " << endl;
		}
	
	}

	else if (choice == 2)
	{
		enemy->skill(enemy, targets);
	}
}

void playTurn(vector<Unit*> player, vector<Unit*> enemy)
{
	vector<Unit*> turnOrder = Order(player, enemy);

	int deathCounterSideA = 0;
	int deathCounterSideB = 0;

	while (true)
	{

		showSides(player);
		cout << endl;
		cout << "VS" << endl;
		cout << endl;
		showSides(enemy);
		cout << endl;

		Unit* doer = turnOrder[0];

		printTurnOrder(turnOrder);

		cout << "Current Turn: " << doer->getName() << endl;
		system("pause");
		system("cls");

		
		
		cout << doer->getName() << endl;
		turnOrder[0]->showStats();
		cout << endl;
	

		if (turnOrder[0]->getSide() == player[0]->getSide())
		{
			playerTurn(doer, turnOrder);
		}
		else
		{
			enemyTurn(doer, turnOrder);
		}
		
		// erases if it's dead & adds to the death counter 
		for (int x = 0; x < turnOrder.size(); x++)
		{
			if (!turnOrder[x]->alive())
			{
				if (turnOrder[x]->getSide() == player[0]->getSide())
				{
					deathCounterSideA++;
				}
				else
				{
					deathCounterSideB++;
				}
				turnOrder.erase(turnOrder.begin() + x);
			}
		}

		if (deathCounterSideA == 3 || deathCounterSideB == 3)
		{
			break;
		}

		rotate(turnOrder.begin(), turnOrder.begin() + 1, turnOrder.end());
		
		system("pause");
		system("cls");
	}
	system("pause");
	system("cls");

	showSides(player);
	cout << endl;
	cout << "VS" << endl;
	cout << endl;
	showSides(enemy);
	cout << endl;

	system("pause");
	system("cls");

	if (deathCounterSideA == 3)
	{
		cout << enemy[0]->getSide() << " won!" << endl;
	}
	else if (deathCounterSideB == 3)
	{
		cout << player[0]->getSide() << " won!" << endl;
	}
}


void showSides(vector<Unit*> &side)
{
	cout << "Team: " << side[0]->getSide() << endl;
	cout << "==============================" << endl;

	for (int a = 0; a < 3; a++)
	{
		if (side[a]->alive())
		{
			cout << side[a]->getName() << "[HP: " << side[a]->getHp() << "/" << side[a]->getMaxHp() << "] " << endl;
		}
		else
		{
			cout << "D E A D" << endl;
		}
	}
}
