#include "WarriorShockwave.h"
#include "Unit.h"


WarriorShockwave::WarriorShockwave(string name) :Skill(name)
{
	mCost = 2;
	mDamageCoefficient = 0.9f;
}

void WarriorShockwave::use(Unit * doer, vector<Unit*>& targets)
{


	cout << doer->getName() << " is using " << doer->getSkillName() << endl;
	for (int i = 0; i < targets.size(); i++)
	{
		if (doer->getSide() != targets[i]->getSide())
		{
			Unit* target = targets[i];
			int damage = doer->computeDamage(doer, target) * mDamageCoefficient;
			target->takeDamage(damage);
			cout << targets[i]->getName() << " took " << damage << " damage. ";
		}
	}
	int cost = getCost();

	doer->decreaseMp(cost);


}


WarriorShockwave::~WarriorShockwave()
{
}
