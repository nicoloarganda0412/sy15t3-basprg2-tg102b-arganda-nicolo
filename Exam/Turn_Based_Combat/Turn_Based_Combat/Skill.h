#pragma once

#include <string>
#include <iostream>
#include <vector>

class Unit;

using namespace std;

class Skill
{
public:
	Skill(string name);
	
	string getName();
	int getCost();

	virtual void use(Unit* player, vector<Unit*> &targets);

	~Skill();

protected: 
	string mName;
	int mCost;
};

